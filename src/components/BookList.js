import * as React from "react";
import { useQuery } from "@apollo/client";

import BookDetails from "./BookDetails";
import { getBooksQuery } from "../queries";

const BookList = () => {
  const [selectedBookId, setSelectedBookId] = React.useState(null);
  const { loading, error, data } = useQuery(getBooksQuery);

  if (loading) return <p>Loading...</p>;
  if (error) return <p>Error :(</p>;

  return (
    <>
      <div className="list-group">
        {data.books.map((book) => (
          <button
            key={book.id}
            type="button"
            className="list-group-item list-group-item-action"
            onClick={() => setSelectedBookId(book.id)}
          >
            {book.name}
          </button>
        ))}
      </div>
      <div className="my-5">
        <h1>Book Details</h1>
        <BookDetails bookId={selectedBookId} />
      </div>
    </>
  );
};

export default BookList;
