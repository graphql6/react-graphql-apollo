import * as React from "react";
import { useQuery } from "@apollo/client";
import { getBookQuery } from "../queries";

const BookDetails = ({ bookId }) => {
  const { data, loading, error } = useQuery(getBookQuery, {
    variables: { bookId },
  });

  if (loading) return "loading";

  if (error) return "Error :(";

  if (!data.book) return <p>No book selected</p>;

  return (
    <div className="mt-4">
      <h5>{data.book.name}</h5>
      <p>
        <b>Genre:</b> {data.book.genre}
      </p>
      <p>
        <b>Author:</b> {data.book.author.name}
      </p>
      <p>Other books by this author.</p>
      <ul>
        {data.book.author.books.map((item) => (
          <li key={item.id}>{item.name}</li>
        ))}
      </ul>
    </div>
  );
};

export default BookDetails;
