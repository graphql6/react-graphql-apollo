import * as React from "react";
import { useQuery, useMutation } from "@apollo/client";

import { getAuthorsQuery, addBookMutation, getBooksQuery } from "../queries";

const AddBook = () => {
  const [book, setBook] = React.useState({
    name: "",
    genre: "",
    authorId: "",
  });
  const { data, loading, error } = useQuery(getAuthorsQuery);
  const [addBook] = useMutation(addBookMutation);

  const handleSubmit = (e) => {
    e.preventDefault();
    addBook({
      variables: { ...book },
      refetchQueries: [{ query: getBooksQuery }],
    });
    setBook({ name: "", genre: "", authorId: "" });
  };

  if (loading) return "loading authors...";

  if (error) return "Failed loading authors.";

  return (
    <form onSubmit={handleSubmit}>
      <div className="mb-3">
        <label className="form-label">Name:</label>
        <input
          className="form-control"
          value={book.name}
          onChange={(e) =>
            setBook((prevState) => ({ ...prevState, name: e.target.value }))
          }
        />
      </div>

      <div className="mb-3">
        <label className="form-label">Genre:</label>
        <input
          className="form-control"
          value={book.genre}
          onChange={(e) =>
            setBook((prevState) => ({ ...prevState, genre: e.target.value }))
          }
        />
      </div>

      <div className="mb-3">
        <label className="form-label">Author:</label>
        <select
          className="form-select"
          value={book.authorId}
          onChange={(e) =>
            setBook((prevState) => ({ ...prevState, authorId: e.target.value }))
          }
        >
          <option value={""}>Select an author</option>
          {data.authors.map((author) => (
            <option key={author.id} value={author.id}>
              {author.name}
            </option>
          ))}
        </select>
      </div>

      <button type="submit" className="btn btn-primary">
        Add
      </button>
    </form>
  );
};

export default AddBook;
