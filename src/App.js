import AddBook from "./components/AddBook";
import BookList from "./components/BookList";

function App() {
  return (
    <div className="row">
      <div className="col-6">
        <h1>Book Listing</h1>
        <BookList />
      </div>
      <div className="col-6">
        <h1>Add Book</h1>
        <AddBook />
      </div>
    </div>
  );
}

export default App;
